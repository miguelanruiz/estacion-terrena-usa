#!/bin/bash

ENV_FILE=/root/.satnogs/config
ENV_FILE_SATNOGS=/etc/default/satnogs-client
ENV_FILE_HAMLIB=/etc/default/hamlib-utils

helpFunction()
{
   echo ""
   echo "Usage: $0 -s True -i StringOfNewParams -o StringOfNewParams"
   echo -e "\t-s Any included argument will reload env for SatNOGS-Client"
   echo -e "\t-i Please include the new RIGCTL configuration"
   echo -e "\t-o Please include the new ROTCTL configuration"
   exit 1 # Exit script after printing help
}

while getopts "s:i:o:" opt
do
   case "$opt" in
      s ) satnogs=true ;;
      i ) rigctl_conf="$OPTARG" ;;
      o ) rotctl_conf="$OPTARG" ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

if [ ! -z "$rigctl_conf" ]; then
	change='RIG_OPTS="'"$rigctl_conf"'"'
	echo "$change"
	sed -i "s/RIG.*/${change}/" $ENV_FILE_HAMLIB
	chown -R hamlib-utils:hamlib-utils $ENV_FILE_HAMLIB
	systemctl restart rigctld
fi

if [ ! -z "$rotctl_conf" ]; then
	change='ROT_OPTS="'"$rotctl_conf"'"'
	echo "$change"
	sed -i "s/ROT.*/${change}/" $ENV_FILE_HAMLIB
	chown -R hamlib-utils:hamlib-utils $ENV_FILE_HAMLIB
	systemctl restart rotctld
fi

if [ "$satnogs" = true ]; then
        echo "Satnogs Enviroment"
        if [ -f "$ENV_FILE" ]; then
        	cat "$ENV_FILE" > $ENV_FILE_SATNOGS
		chown -R satnogs:satnogs $ENV_FILE_SATNOGS
        	systemctl restart satnogs-client
        fi     
fi

