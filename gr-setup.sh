#!/bin/bash

PWD=$(pwd)
SATNOGS_ROOT_DIR=/home/satnogs
SATNOGS_DOWNLOAD_DIR="${PWD}/download"
SATNOGS_PACKAGES_DIR="${PWD}/packages"
SATNOGS_CONFIG_DIR=/usr/local/lib/python3.8/dist-packages/satnogsconfig/
SATNOGS_ANSIBLE_DIR=/root/.satnogs/ansible/
INSTALLER_LOG_FILE="${PWD}/ground_station_usa.log"
DEFAULT_ENV_DIR=/etc/default/
SYSTEMD_FILES_DIR=/etc/systemd/system/

err_report() {
    echo "Error on line $1"
    echo "Please take in account ground_station_usa.log file. Installer stopped..."
    exit
}

trap 'err_report $LINENO' ERR

echo
echo
echo $'                            ▄▄▄▄▄▄▄                                █████▄'
echo $'                  ▄▄▄██████████████████████▄▄▄                   ▐████████'
echo $'              ▄███████████████████████████████████▄▄             █████████▌'
echo $'           ▄███████████████████████████████████████████▄         ▐████████'
echo $'        ▄██████████████████████████████████████████████████▄    ▄████▀▀▀`'
echo $'      ▄████████████████████████████████████████████████████▀   ████▀'
echo $'     █████████████████████████████████████████████████████   ▄███▀'
echo $'   ▄████████████████████████████████████████████████████▀   ████   ▄▄'
echo $'  ▐████████████████████████████████████████████████████   ████▀   ████▄ '
echo $'  ███████████████████████████████████████████████████▀  ▄████   ▄████████ '
echo $' ██████████████████████████████████████████████████▀   ████▀  ▄████████████ '
echo $'▐█████████████████████████████████████████████████   ▄████   ████████████████'
echo $'████████████████████████████████████████████████▀   ████▀  ▄██████████████████▄'
echo $'███████████████████████████████████████████████   ▄███▀   ██████████████████████'
echo $'█████████████████████████████████████████████▀  ▄████   ▄████████████████████████▄'
echo $'████████████████████████████████████████████   ████▀   ███████████████████████████▄'
echo $'█████████████████████████████████████████▀  ▄████   ██████████████████████████████▌'
echo $'███████████████████████████████████████▀   ████▀  ▄████████████████████████████████▌'
echo $' ██████████████████████████████████████   ▄███▀   ███████████████████████████████████▄'
echo $'  █████████████████████████████████████  ▐███   ▄█████████████████████████████████████'
echo $'  ▐█████████████████████████████████████       ████████████████████████████████████████'
echo $'   ████████████████████████████████████████████████████████████████████████████████████ '
echo $'    ████████████████████████████████████████████████████████████████████████████████████'
echo $'     ███████████████████████████████████████████████████████████████████████████████████'
echo $'      ▀█████████████████████████████████████████████████████████████████████████████████'
echo $'        ████████████████████████████████████████████████████████████████████████████████'
echo $'         ███████████████████████████████████████████████████████████████████████████████'
echo $'           █████████████████████████████████████████████████████████████████████████████'
echo $'            ▀██████████████████████████████████████████████████████████████████████████ '
echo $'              ▀████████████████████████████████████████████████████████████████████████'
echo $'                ▀█████████████████████████████████████████████████████████████████████'
echo $'                  ▀██████████████████████████████████████████████████████████████████'
echo $'                     ▀█████████████████████████████████████████████████████████████▀'
echo $'                        ▀█████████████████████████████████████████████████████████▀'
echo $'                           ▀████████████████████████████████████████████████████▀'
echo $'                              ▀▀█████████████████████████████████████████████▀ '
echo $'                                   ▀██████████████████████████████████████▀▀'
echo $'                                        ▀▀████████████████████████████▀▀'
echo $'                                                ▀▀▀▀▀▀▀███▀▀▀▀▀▀▀'
echo $''
echo $'     ▄▄                                                      ▄▄                ▄▄            ▄▄  '
echo $' ▄█████████                  ████     ▐████▌    ████    ▄██████████▄      ▄██████████    ▄█████████'
echo $'▐███      ▀     ▄▄▄▄▄▄▄▄    ▄████▄▄▄  ▐██████   ████   ████▀    ▀████   ▄████       ▀   ▐███      ▀'
echo $' █████▄▄▄▄     ▐▀▀▀▀▀▀███   ▀████▀▀▀  ▐███▀███  ████   ███▌      ▐███▌  ████             █████▄▄▄▄'
echo $'  ▀▀▀███████    ▄▄███████▌   ████     ▐███  ███ ████  ▐███▌      ▐███▌ ▐███▌    █████▌    ▀▀▀██████▄'
echo $'        ████  ▐███▀   ███▌   ████     ▐███   ███████   ████      ████   ████▄     ███▌          ████'
echo $' ██████████▀  ▐████▄▄████▌   ▀██████  ▐███    ▀█████    ▀███████████     ▀██████▄████▌  ▐████▄█████▀'
echo $'  ▀▀▀▀▀▀▀▀      ▀▀▀▀▀ ▀▀▀▀     ▀▀▀▀▀   ▀▀▀     ▀▀▀▀▀       ▀▀▀▀▀▀▀          ▀▀▀▀▀▀▀▀      ▀▀▀▀▀▀▀▀'

echo "		Welcome to automated Ground Station USA based on SatNOGS Network installation software."

sleep 10

if [ "$EUID" -ne 0 ]
  then echo "Please run as root."
  exit
else
  echo "You are root, perfect!"
fi

source /etc/os-release

if [ "$VERSION_ID" = "20.04" ]
then
  echo "Operating system satisfied ${PRETTY_NAME}."
else 
  echo "This operating system is not compatible with automated installer."
  exit
fi

if ping -q -c 1 -W 1 8.8.8.8 >/dev/null; then
  echo "Internet connection is available. Great!"
else
  echo "Internet connection is not available. Aborting..."
  sleep 2
  exit
fi

#echo
#echo
#echo "Creating users..."
#echo
#echo
#getent group satnogs > /dev/null 2>&1 || addgroup --system satnogs
#getent passwd satnogs > /dev/null 2>&1 || adduser --system --ingroup satnogs --home /home/satnogs satnogs

#getent group hamlib-utils > /dev/null 2>&1 || addgroup --system hamlib-utils
#getent passwd hamlib-utils > /dev/null 2>&1 || adduser --system --ingroup hamlib-utils --no-create-home hamlib-utils

sleep 1

echo
echo "Updating APT Repositories..."
echo

apt update

echo
echo "Installing packages from APT Repositories..."
echo
apt-get install -y libboost-dev libboost-date-time-dev libboost-filesystem-dev libboost-program-options-dev libboost-system-dev libboost-thread-dev libboost-regex-dev libboost-test-dev swig cmake build-essential pkg-config gnuradio-dev libconfig++-dev libgmp-dev liborc-0.4-0 liborc-0.4-dev liborc-0.4-dev-bin nlohmann-json3-dev libpng++-dev libvorbis-dev git python3-libhamlib2 python3-dialog python3 python3-six python3-mako python3-soapysdr python3-dev gcc gnuradio-dev gnuradio libsoapysdr-dev libconfig++-dev libgmp-dev liborc-0.4-0 liborc-0.4-dev liborc-0.4-dev-bin python3-pip soapysdr-tools rtl-sdr librtlsdr-dev libxml2 libxml2-dev bison flex libaio-dev libboost-all-dev libgmp-dev liborc-0.4-dev libusb-1.0-0-dev libserialport-dev graphviz doxygen g++ python3-numpy python3-sphinx python3-lxml libfftw3-dev libsdl1.2-dev libgsl-dev libqwt-qt5-dev libqt5opengl5-dev python3-pyqt5 liblog4cpp5-dev libzmq3-dev python3-yaml python3-click python3-click-plugins python3-zmq python3-scipy python3-gi python3-gi-cairo gobject-introspection gir1.2-gtk-3.0 libitpp-dev libhackrf-dev hackrf openssh-server sshpass ansible neovim python3-all >> "${INSTALLER_LOG_FILE}"

sleep 1

echo "Providing OpenSSH Server config for automated Ansible software..."

echo "Port 22" >> /etc/ssh/sshd_config
echo "ListenAddress 0.0.0.0" >> /etc/ssh/sshd_config
ssh-keygen -l -E md5 -f <(cat /etc/ssh/ssh_host_*_key.pub) 2>/dev/null
ssh-keygen -l -E sha256 -f <(cat /etc/ssh/ssh_host_*_key.pub) 2>/dev/null
systemctl restart sshd

echo "Current OpenSSH Server service status is: $(systemctl is-active sshd)"

echo
echo "Downloading Hamlib-utils 4.4."
echo

if [ ! -d "$SATNOGS_DOWNLOAD_DIR" ]; then
	mkdir $SATNOGS_DOWNLOAD_DIR
fi

cd "${SATNOGS_DOWNLOAD_DIR}"

wget https://github.com/Hamlib/Hamlib/releases/download/4.4/hamlib-4.4.tar.gz > /dev/null 2>&1
tar xvf hamlib-4.4.tar.gz > /dev/null 2>&1 
cd hamlib-4.4
./configure --prefix=/usr --with-python-binding PYTHON="/usr/bin/python3" >> "${INSTALLER_LOG_FILE}"
make > /dev/null 2>&1
make install
echo "Installed Hamlib-utils 4.4."
ldconfig
cd ..

echo "Getting libgpredict for satnogs-monitor..."
git clone https://github.com/cubehub/libgpredict.git
cd libgpredict
mkdir build
cd build
cmake ../ >> "${INSTALLER_LOG_FILE}"
make > /dev/null 2>&1
make install
echo "Installed libgpredict successful"
sudo ldconfig > /dev/null 2>&1

sleep 1

echo
cd "${SATNOGS_PACKAGES_DIR}"
dpkg -i libgnuradio-soapy_2.1.3.1-1_amd64.deb
dpkg -i gr-soapy_2.1.3.1-1_amd64.deb 
echo "Package installed: $(dpkg-query --show | grep gr-soapy)"

sleep 1

echo
dpkg -i libgnuradio-satnogs_2.3.1.1-1_amd64.deb 
dpkg -i gr-satnogs_2.3.1.1-1_amd64.deb
echo "Package installed: $(dpkg-query --show | grep gr-satnogs)"

sleep 1

echo
echo "Installing SatNOGS Flowgraphs v1.4-1 from compiled source code..."
echo

cd "${SATNOGS_PACKAGES_DIR}"
dpkg -i satnogs-flowgraphs_1.4-1_all.deb
echo "Package installed: $(dpkg-query --show | grep flowgraphs)"
echo "Installing python3-gps (gpsd module)."
dpkg -i python3-gps_3.20-8ubuntu0.4_amd64.deb
echo "Package installed: $(dpkg-query --show | grep python3-gps)"
echo "Installing satnogs-monitor by wose."
dpkg -i satnogs-monitor_0.4.2_amd64.deb
echo "Package installed: $(dpkg-query --show | grep satnogs-monitor)"
cd ..

sleep 1

echo
echo
echo "Installing packages through Ansible provisioning software..."
echo "Please insert user password next to this..."
echo

cd "${PWD}/ansible"
ansible-playbook -i production/inventory/hosts -K site.yml

if [ ! -d "$SATNOGS_ANSIBLE_DIR" ]; then
        mkdir $SATNOGS_ANSIBLE_DIR
fi

echo "Updating Ansible provisioning software files..."

cp -r ./* $SATNOGS_ANSIBLE_DIR

sleep 1

echo
echo
echo "Starting daemon for installed services..."
echo
echo

systemctl restart satnogs-client
systemctl restart rigctld

sleep 2

COUNT_ERRORS=0

if [ ! $(systemctl is-active rigctld) = "active" ];
then
	echo "rigctld service is not running well.."
	(( COUNT_ERRORS++ ))
fi

if [ ! $(systemctl is-active satnogs-client) = "active" ];
then
	echo "satnogs-client service is not running well.."
	(( COUNT_ERRORS++ ))
fi

if [ ! $COUNT_ERRORS=0 ];then
	echo "Some errors starting services, have to be checked before start ground station. Installation is completed under revision."
	exit
fi

echo
echo
echo "Installation complete without errors and next settings:"
echo
cat /etc/default/satnogs-client
echo
echo
echo "Now you can run satnogs-setup software and set config that you need for Ground Station USA. Instead, we suggest restart the machine."
echo "Please set parameters to start working rotator with Ground Station USA."
echo
echo

